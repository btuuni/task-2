package com.university.task_2

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_second.*

class MainActivity : AppCompatActivity() {

    private val REQUEST_CODE = 69

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        profileChangeButton.setOnClickListener {
            openSecondActivity()

        }
    }

    private fun openSecondActivity() {
        val userModel = UserModel(
            nameTextView.text.toString(),
            surnameTextView.text.toString(),
            emailTextView.text.toString(),
            dateofbirthTextView.text.toString().toInt(),
            sexTextView.text.toString()
        )

        val intent = Intent(this, SecondActivity::class.java)
        intent.putExtra("userModel", userModel)
        startActivityForResult(intent, REQUEST_CODE)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE) {
            val name = data!!.extras!!.getString("name", "")
            val surname = data!!.extras!!.getString("surname", "")
            val mail = data!!.extras!!.getString("mail", "")
            val dateofbirth = data!!.extras!!.getInt("dateofbirth", 0)
            val sex = data!!.extras!!.getString("sex", "")
            nameTextView.text = name.toString()
            surnameTextView.text = surname.toString()
            emailTextView.text = mail.toString()
            dateofbirthTextView.text = dateofbirth.toString()
            sexTextView.text = sex.toString()


        }
        super.onActivityResult(requestCode, resultCode, data)
    }
}
