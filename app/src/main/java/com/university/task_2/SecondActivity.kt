package com.university.task_2

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_second.*

class SecondActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        init()
    }

    private fun init() {
        val intent = intent
        val userModel = intent.getParcelableExtra("userModel") as UserModel
        editName.setText(userModel.name)
        editSurname.setText(userModel.surname)
        editMail.setText(userModel.mail)
        editDateOfBirth.setText(userModel.dateofbirth.toString())
        editSex.setText(userModel.sex)

        saveButton.setOnClickListener {
            backToMain(userModel)
        }
    }

    private fun backToMain(userModel: UserModel) {
        val intent = intent

        intent.putExtra("name", editName.text.toString())
        intent.putExtra("surname", editSurname.text.toString())
        intent.putExtra("mail", editMail.text.toString())
        intent.putExtra("dateofbirth", editDateOfBirth.text.toString().toInt())
        intent.putExtra("sex", editSex.text.toString())
        setResult(Activity.RESULT_OK, intent)
        finish()
    }
}
